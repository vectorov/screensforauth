using Foundation;
using System;
using UIKit;
using System.Collections.Generic;

namespace ScreensForAuth.iOS
{
    public partial class RegistrationController : UIViewController, IUITableViewDelegate, IUITableViewDataSource
    {
		private NSMutableArray ArrayConfigCell;

		private List<NSObject> observers;
		private const string EnterObserverName = "EnterButtonTouch";
		private const string RegisterObserverName = "RegisterButtonTouch";

		public RegistrationController (IntPtr handle) : base (handle)
        {
        }
		#region Controller life cycle
		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			// Perform any additional setup after loading the view, typically from a nib.

			tableView.WeakDelegate = this;
			tableView.WeakDataSource = this;

			RegisterNibs();

			CreateConfigCell();

		}

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);

			observers = new List<NSObject>();
			observers.Add(NSNotificationCenter.DefaultCenter.AddObserver((NSString)EnterObserverName, ObserversHandler));
			observers.Add(NSNotificationCenter.DefaultCenter.AddObserver((NSString)RegisterObserverName, ObserversHandler));
		}

		public override void ViewWillDisappear(bool animated)
		{
			base.ViewWillDisappear(animated);

			NSNotificationCenter.DefaultCenter.RemoveObservers(observers);
		}
		#endregion

		#region Observers handlers
		void ObserversHandler(NSNotification notification)
		{
			switch (notification.Name)
			{
				case EnterObserverName:
					//PerformSegue("ToFogotPasswordController", null);
					break;
				case RegisterObserverName:
					PerformSegue("ToRegistrationController", null);
					break;
				default:
					break;
			}
		}
		#endregion

		private void RegisterNibs()
		{
			tableView.RegisterNibForCellReuse(TextFieldCell.Nib, TextFieldCell.Key);

			tableView.RegisterNibForCellReuse(ButtonCell.Nib, ButtonCell.Key);

			tableView.RegisterNibForCellReuse(SwitchWithLabelCell.Nib, SwitchWithLabelCell.Key);

			tableView.RegisterNibForCellReuse(ButtonWithAttrTitleCell.Nib, ButtonWithAttrTitleCell.Key);

			tableView.RegisterNibForCellReuse(PhoneNumberCell.Nib, PhoneNumberCell.Key);
		}

		void CreateConfigCell()
		{
			ArrayConfigCell = new NSMutableArray();

			ArrayConfigCell.Add(new TextFieldConfig(TextFieldType.Name));
			ArrayConfigCell.Add(new TextFieldConfig(TextFieldType.Surname));
			ArrayConfigCell.Add(new TextFieldConfig(TextFieldType.Email));
			ArrayConfigCell.Add(new TextFieldConfig(TextFieldType.Country));
		}

		public nint RowsInSection(UITableView tableView, nint section)
		{
			return 9;
		}

		public UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
			if (indexPath.Row <= 3)
			{
				var cell = (TextFieldCell)tableView.DequeueReusableCell(TextFieldCell.Key, indexPath);
				var config = ArrayConfigCell.GetItem<TextFieldConfig>((nuint)indexPath.Row);
				cell.Config(config);

				return cell;
			}

			if (indexPath.Row == 4)
			{
				var cell = (PhoneNumberCell)tableView.DequeueReusableCell(PhoneNumberCell.Key, indexPath);

				return cell;
			}

			if (indexPath.Row == 5)
			{
				var cell = (SwitchWithLabelCell)tableView.DequeueReusableCell(SwitchWithLabelCell.Key, indexPath);

				return cell;
			}

			if (indexPath.Row == 6)
			{
				var cell = (ButtonCell)tableView.DequeueReusableCell(ButtonCell.Key, indexPath);

				var config = new ButtonConfig(ButtonType.Register);
				config.Color = UIColor.Green;

				cell.ConfigButton(config);

				return cell;
			}

			if (indexPath.Row == 7)
			{
				var cell = (ButtonWithAttrTitleCell)tableView.DequeueReusableCell(ButtonWithAttrTitleCell.Key, indexPath);

				var attr = new UIStringAttributes() { UnderlineStyle = NSUnderlineStyle.None, ForegroundColor = UIColor.LightGray };
				NSAttributedString title = new NSAttributedString("AlreadyHaveAccount".FromResource(), attr);

				cell.SetLabelTitle(title);

				return cell;
			}

			if (indexPath.Row == 8)
			{
				var cell = (ButtonCell)tableView.DequeueReusableCell(ButtonCell.Key, indexPath);

				var config = new ButtonConfig(ButtonType.Enter);
				config.Color = UIColor.Blue;

				cell.ConfigButton(config);

				return cell;
			}

			return null;
		}

		[Export("tableView:heightForRowAtIndexPath:")]
		public nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
		{
			nfloat height = 80f;
			switch (indexPath.Row)
			{
				case 0:
					height = 50f;
					break;
				case 1:
					height = 50f;
					break;
				case 2:
					height = 50f;
					break;
				case 3:
					height = 50f;
					break;
				case 4:
					height = 50f;
					break;
				case 5:
					height = 60f;
					break;
				case 6:
					height = 50f;
					break;
				case 7:
					height = 60f;
					break;
				case 8:
					height = 50f;
					break;
				default:
					break;
			}

			return height;
		}

	}
}