﻿using System;
using System.Collections.Generic;
using CoreGraphics;
using Foundation;
using UIKit;

namespace ScreensForAuth.iOS
{
	public partial class AuthorizationController : UIViewController, IUITableViewDelegate, IUITableViewDataSource
	{
		private NSMutableArray ArrayConfigCell;

		public AuthorizationController(IntPtr handle) : base(handle)
		{
		}

		public AuthorizationController() : base("AuthorizationController", null)
		{
		}

		#region Controller life cycle
		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			// Perform any additional setup after loading the view, typically from a nib.

			tableView.WeakDelegate = this;
			tableView.WeakDataSource = this;

			RegisterNibs();

			CreateConfigCell();

		}

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);
		}

		public override void ViewWillDisappear(bool animated)
		{
			base.ViewWillDisappear(animated);
		}
		#endregion

		#region Buttons handlers
		void SegueToRegisterController()
		{
			PerformSegue("ToRegistrationController", null);
		}

		void SegueToFogotPasswordController()
		{
			PerformSegue("ToFogotPasswordController", null);
		}

		void AuthorizeAction()
		{
			
		}

		void SocialAuthorizeAction(SocialButtonName buttonName)
		{
			if (buttonName == SocialButtonName.Facebook)
			{ }
			else if (buttonName == SocialButtonName.Vkontakte)
			{ }
			else if (buttonName == SocialButtonName.Odnoklassniki)
			{ }
			else if (buttonName == SocialButtonName.GooglePlus)
			{ }
		}

		#endregion

		private void RegisterNibs()
		{ 
			tableView.RegisterNibForCellReuse(SocialButtonsCell.Nib, SocialButtonsCell.Key);

			tableView.RegisterNibForCellReuse(TextFieldCell.Nib, TextFieldCell.Key);

			tableView.RegisterNibForCellReuse(ButtonCell.Nib, ButtonCell.Key);

			tableView.RegisterNibForCellReuse(SwitchWithLabelCell.Nib, SwitchWithLabelCell.Key);

			tableView.RegisterNibForCellReuse(ButtonWithAttrTitleCell.Nib, ButtonWithAttrTitleCell.Key);

			//tableView.TableHeaderView = TableViewHeaderView.Create(); //new TableViewHeaderView(new CGRect(0,0, 320, 50));
		}

		void CreateConfigCell()
		{
			ArrayConfigCell = new NSMutableArray();

			ArrayConfigCell.Add(new TextFieldConfig(TextFieldType.Email));
			ArrayConfigCell.Add(new TextFieldConfig(TextFieldType.Password));
		}

		public override void DidReceiveMemoryWarning()
		{
			base.DidReceiveMemoryWarning();
			// Release any cached data, images, etc that aren't in use.
		}

		public nint RowsInSection(UITableView tableView, nint section)
		{
			return 7;
		}

		public UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
			if (indexPath.Row == 0 || indexPath.Row == 1)
			{
				var cell = (TextFieldCell)tableView.DequeueReusableCell(TextFieldCell.Key, indexPath);
				var config = ArrayConfigCell.GetItem<TextFieldConfig>((nuint)indexPath.Row);
				cell.Config(config);

				return cell;
			}

			if (indexPath.Row == 2)
			{ 
				var cell = (ButtonCell)tableView.DequeueReusableCell(ButtonCell.Key, indexPath);

				var config = new ButtonConfig(ButtonType.Enter);
				cell.touch = AuthorizeAction;

				cell.ConfigButton(config);

				return cell;
			}

			if (indexPath.Row == 3)
			{
				var cell = (ButtonWithAttrTitleCell)tableView.DequeueReusableCell(ButtonWithAttrTitleCell.Key, indexPath);
				cell.touch = SegueToFogotPasswordController;

				return cell;
			}

			if (indexPath.Row == 4)
			{
				var cell = (SocialButtonsCell)tableView.DequeueReusableCell(SocialButtonsCell.Key, indexPath);
				cell.Touch = SocialAuthorizeAction;

				return cell;
			}

			if (indexPath.Row == 5)
			{
				var cell = (ButtonWithAttrTitleCell)tableView.DequeueReusableCell(ButtonWithAttrTitleCell.Key, indexPath);

				var attr = new UIStringAttributes() { UnderlineStyle = NSUnderlineStyle.None, ForegroundColor = UIColor.LightGray };
				NSAttributedString title = new NSAttributedString("NoAccountTitle".FromResource(), attr);

				cell.SetLabelTitle(title);

				return cell;
			}

			if (indexPath.Row == 6)
			{
				var cell = (ButtonCell)tableView.DequeueReusableCell(ButtonCell.Key, indexPath);

				var config = new ButtonConfig(ButtonType.Register);

				cell.ConfigButton(config);
				cell.touch = SegueToRegisterController;

				return cell;
			}

			return null;
		}

		[Export("tableView:heightForRowAtIndexPath:")]
		public nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
		{
			nfloat height = 80f;

			nfloat heightForNoAccount = 50f;

			if (UIScreen.MainScreen.Bounds.Size.Width <= 320)
			{
				heightForNoAccount = 30f;
			}

			switch (indexPath.Row)
			{
				case 0:
					height = 50f;
					break;
				case 1:
					height = 50f;
					break;
				case 2:
					height = 60f;
					break;
				case 3:
					height = 50f;
					break;
				case 4:
					height = GetHeightForSocialButtons();//130f;
					break;
				case 5:
					height = heightForNoAccount;
					break;
				case 6:
					height = 60f;
					break;
				default:
					break;
			}

			return height;
		}

		[Export("tableView:viewForHeaderInSection:")]
		public UIView GetViewForHeader(UITableView tableView, nint section)
		{
			UIView v = new UIView();

			UILabel l = new UILabel(new CGRect(10, 20, UIScreen.MainScreen.Bounds.Width - 20, 70));
			l.Lines = 0;
			l.PreferredMaxLayoutWidth = UIScreen.MainScreen.Bounds.Width;
			l.Text = "RegisterHeaderTitle".FromResource();
			l.TextAlignment = UITextAlignment.Center;
			l.SizeToFit();

			v.AddSubview(l);

			return v;
		}

		[Export("tableView:heightForHeaderInSection:")]
		public nfloat GetHeightForHeader(UITableView tableView, nint section)
		{
			return 70f;
		}

		nfloat GetHeightForSocialButtons()
		{
			var margin = 320 / 16;
			var w = (320 - margin * 5) / 4;
			return w + 80;
		}

	}
}

