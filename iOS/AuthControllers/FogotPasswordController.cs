﻿using System;
using Foundation;
using UIKit;

namespace ScreensForAuth.iOS
{
	public partial class FogotPasswordController : UIViewController, IUITableViewDelegate, IUITableViewDataSource
	{
		private NSMutableArray ArrayConfigCell;

		public FogotPasswordController(IntPtr handle) : base(handle)
		{
		}

		public FogotPasswordController() : base("FogotPasswordController", null)
		{
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			tableView.WeakDelegate = this;
			tableView.WeakDataSource = this;

			RegisterNibs();

			CreateConfigCell();

			tableView.ContentInset = new UIEdgeInsets((UIScreen.MainScreen.Bounds.Height - NavigationController.NavigationBar.Bounds.Size.Height) / 2 - 100, 0, 0 ,0);

		}

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);
		}

		private void RegisterNibs()
		{
			tableView.RegisterNibForCellReuse(TextFieldCell.Nib, TextFieldCell.Key);

			tableView.RegisterNibForCellReuse(ButtonCell.Nib, ButtonCell.Key);
		}

		void CreateConfigCell()
		{
			ArrayConfigCell = new NSMutableArray();

			ArrayConfigCell.Add(new TextFieldConfig(TextFieldType.Email));
		}

		public override void DidReceiveMemoryWarning()
		{
			base.DidReceiveMemoryWarning();
			// Release any cached data, images, etc that aren't in use.
		}

		public nint RowsInSection(UITableView tableView, nint section)
		{
			return 2;
		}

		public UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
			if (indexPath.Row == 0)
			{
				var cell = (TextFieldCell)tableView.DequeueReusableCell(TextFieldCell.Key, indexPath);
				var config = ArrayConfigCell.GetItem<TextFieldConfig>((nuint)indexPath.Row);
				cell.Config(config);

				return cell;
			}

			if (indexPath.Row == 1)
			{
				var cell = (ButtonCell)tableView.DequeueReusableCell(ButtonCell.Key, indexPath);

				var config = new ButtonConfig(ButtonType.Next);

				cell.ConfigButton(config);

				return cell;
			}

			return null;
		}

		[Export("tableView:heightForRowAtIndexPath:")]
		public nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
		{
			nfloat height = 80f;
			switch (indexPath.Row)
			{
				case 0:
					height = 50f;
					break;
				case 1:
					height = 60f;
					break;
				default:
					break;
			}

			return height;
		}
	}
}

