﻿using Foundation;

namespace ScreensForAuth.iOS
{
	public static class Tools
	{
		public static string FromResource(this string key)
		{
			return NSBundle.MainBundle.LocalizedString(key, "");
		}
	}
}
