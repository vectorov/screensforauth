﻿using System;
using CoreGraphics;
using Foundation;
using UIKit;

namespace ScreensForAuth.iOS
{
	public partial class ViewController : UIViewController, IUICollectionViewDataSource, IUICollectionViewDelegateFlowLayout
	{
		private bool IsCheckCustomerAgreement;

		public ViewController(IntPtr handle) : base(handle)
		{
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			RegisterCollectionView.WeakDelegate = this;
			RegisterCollectionView.WeakDataSource = this;

			RegisterNibs();

		}

		private void RegisterNibs()
		{ 
			var cellNib = UINib.FromName(RegistrationCell.Key, NSBundle.MainBundle);
			RegisterCollectionView.RegisterNibForCell(cellNib, RegistrationCell.Key);
		}

		public override void DidReceiveMemoryWarning()
		{
			base.DidReceiveMemoryWarning();
			// Release any cached data, images, etc that aren't in use.		
		}

		nint IUICollectionViewDataSource.GetItemsCount(UICollectionView collectionView, nint section)
		{
			return 1;
		}

		UICollectionViewCell IUICollectionViewDataSource.GetCell(UICollectionView collectionView, NSIndexPath indexPath)
		{
			var cell = (RegistrationCell) RegisterCollectionView.DequeueReusableCell(RegistrationCell.Key, indexPath);
			cell?.SetupView();
			return cell;
		}

		[Export("numberOfSectionsInCollectionView:")]
		public nint NumberOfSections(UICollectionView collectionView)
		{
			return 1;
		}

		[Export("collectionView:layout:sizeForItemAtIndexPath:")]
		public CoreGraphics.CGSize GetSizeForItem(UICollectionView collectionView, UICollectionViewLayout layout, NSIndexPath indexPath)
		{
			return new CGSize(UIScreen.MainScreen.Bounds.Size.Width, 600);
		}
	}
}
