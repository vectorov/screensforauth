using Foundation;
using System;
using UIKit;
using ObjCRuntime;
using CoreGraphics;

namespace ScreensForAuth.iOS
{
    public partial class TableViewHeaderView : UIView
    {
		public static readonly NSString Key = new NSString("TableViewHeaderView");
		public static readonly UINib Nib;

		static TableViewHeaderView()
		{
			Nib = UINib.FromName("TableViewHeaderView", NSBundle.MainBundle);
		}

		public TableViewHeaderView (IntPtr handle) : base (handle)
        {
        }

		//public TableViewHeaderView(CGRect frame) : base(frame)
		//{
		//	this.Frame = frame;

		//}

		public static TableViewHeaderView Create()
		{
			var arr = NSBundle.MainBundle.LoadNib("TableViewHeaderView", null, null);
			var v = Runtime.GetNSObject<TableViewHeaderView>(arr.ValueAt(0));

			//CGRect rect = v.Frame;
			//rect.Width = UIScreen.MainScreen.Bounds.Size.Width;
			//rect.Height = 0f;

			v.Frame = new CGRect(0,0, UIScreen.MainScreen.Bounds.Size.Width, 120);

			return v;
		}
}
}