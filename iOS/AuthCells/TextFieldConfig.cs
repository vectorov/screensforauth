﻿using Foundation;
using UIKit;

namespace ScreensForAuth.iOS
{
	public enum TextFieldType
	{
		Name = 0,
		Surname = 1,
		Login = 2,
		Country = 3,
		Email = 4,
		Password = 5
	}

	public class TextFieldConfig : NSObject
	{
		public string Placeholder { get; set; }
		public UIKeyboardType KeyboardType { get; set; }
		public TextFieldType Type { get; set; }
		public string Value { get; set; }
		public UIView RightView { get; set; }
		public UIView LeftView { get; set; }
		public UITextFieldViewMode RightViewMode { get; set; }
		public UITextFieldViewMode LeftViewMode { get; set; }
		public bool Secure { get; set; }

		public TextFieldConfig(TextFieldType type)
		{
			Type = type;
			Secure = false;

			switch (Type)
			{
				case TextFieldType.Name:
					Placeholder = "NamePlaceholder".FromResource();
					KeyboardType = UIKeyboardType.AsciiCapable;
					break;
				case TextFieldType.Surname:
					Placeholder = "SurnamePlaceholder".FromResource();
					KeyboardType = UIKeyboardType.AsciiCapable;
					break;
				case TextFieldType.Country:
					Placeholder = "CountryPlaceholder".FromResource();
					KeyboardType = UIKeyboardType.AsciiCapable;
					break;
				case TextFieldType.Email:
					Placeholder = "EmailPlaceholder".FromResource();
					KeyboardType = UIKeyboardType.EmailAddress;
					break;
				case TextFieldType.Password:
					Placeholder = "PasswordPlaceholder".FromResource();
					KeyboardType = UIKeyboardType.AsciiCapable;
					Secure = true;
					break;
				default:
					break;
			}
		}
	}
}
