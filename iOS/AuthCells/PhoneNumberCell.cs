﻿using System;
using CoreAnimation;
using CoreGraphics;
using Foundation;
using UIKit;

namespace ScreensForAuth.iOS
{
	public partial class PhoneNumberCell : UITableViewCell
	{
		public static readonly NSString Key = new NSString("PhoneNumberCell");
		public static readonly UINib Nib;

		static PhoneNumberCell()
		{
			Nib = UINib.FromName("PhoneNumberCell", NSBundle.MainBundle);
		}

		protected PhoneNumberCell(IntPtr handle) : base(handle)
		{
			// Note: this .ctor should not contain any initialization logic.
		}

		public override void AwakeFromNib()
		{
			base.AwakeFromNib();

			OperatorCodeTextField.Placeholder = "OperatorCode_RegistrPlaceholder".FromResource();
			PhoneNumberTextField.Placeholder = "PhoneNumber_RegistrPlaceholder".FromResource();
		}

		public override void Draw(CGRect rect)
		{
			base.Draw(rect);

			var MaxY = CountryCodeTextField.Frame.Y + CountryCodeTextField.Frame.Height + 2;

			var startingPoint1 = new CGPoint(rect.GetMinX() + 10, MaxY);
			var endingPoint1 = new CGPoint(CountryCodeTextField.Frame.GetMaxX(), MaxY);

			var startingPoint2 = new CGPoint(OperatorCodeTextField.Frame.GetMinX(), MaxY);
			var endingPoint2 = new CGPoint(OperatorCodeTextField.Frame.GetMaxX(), MaxY);

			var startingPoint3 = new CGPoint(PhoneNumberTextField.Frame.GetMinX(), MaxY);
			var endingPoint3 = new CGPoint(rect.GetMaxX() - 10, MaxY);

			var path = new UIBezierPath();
			path.LineWidth = 1.0f;

			path.MoveTo(startingPoint1);
			path.AddLineTo(endingPoint1);

			path.MoveTo(startingPoint2);
			path.AddLineTo(endingPoint2);

			path.MoveTo(startingPoint3);
			path.AddLineTo(endingPoint3);

			UIColor.LightGray.SetStroke();

			path.Stroke();
		}
	}

}
