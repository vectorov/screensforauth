﻿using System;

using Foundation;
using UIKit;

namespace ScreensForAuth.iOS
{
	public partial class ButtonWithAttrTitleCell : UITableViewCell
	{
		public static readonly NSString Key = new NSString("ButtonWithAttrTitleCell");
		public static readonly UINib Nib;

		public Action touch;

		static ButtonWithAttrTitleCell()
		{
			Nib = UINib.FromName("ButtonWithAttrTitleCell", NSBundle.MainBundle);
		}

		protected ButtonWithAttrTitleCell(IntPtr handle) : base(handle)
		{
			// Note: this .ctor should not contain any initialization logic.
		}

		public void SetLabelTitle(NSAttributedString title)
		{
			Button.SetAttributedTitle(title, UIControlState.Normal);
		}

		partial void Button_TouchUpInside(UIButton sender)
		{
			if (touch != null)
			{
				touch();
			}
		}
	}
}
