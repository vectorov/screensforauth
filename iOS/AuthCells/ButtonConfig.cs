﻿using System;
using UIKit;

namespace ScreensForAuth.iOS
{
	public enum ButtonType
	{
		Enter = 0,
		Register = 1,
		Next = 2,
	}

	public class ButtonConfig
	{
		public ButtonType Type { get; set; }
		public UIColor Color { get; set; }
		public string Title { get; set; }
		public string NotificationName { get; set; }
		public nfloat WidthInPercent { get; set; }

		public ButtonConfig(ButtonType type)
		{
			Type = type;
			if (UIDevice.CurrentDevice.Model.ToLower().Contains("ipad"))
			{ 
				WidthInPercent = 0.5f;
			}
			else
			{
				WidthInPercent = 0.875f;
			}

			switch (Type)
			{
				case ButtonType.Enter:
					Title = "EnterButtonTitle".FromResource();
					Color = UIColor.Green;
					break;
				case ButtonType.Register:
					Title = "RegisterButtonTitle".FromResource();
					Color = UIColor.Blue;
					break;
				case ButtonType.Next:
					Title = "NextButtonTitle".FromResource();
					Color = UIColor.Green;
					break;
				default:
					break;
			}
		}
	}
}
