// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace ScreensForAuth.iOS
{
    [Register ("PhoneNumberCell")]
    partial class PhoneNumberCell
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField CountryCodeTextField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField OperatorCodeTextField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField PhoneNumberTextField { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (CountryCodeTextField != null) {
                CountryCodeTextField.Dispose ();
                CountryCodeTextField = null;
            }

            if (OperatorCodeTextField != null) {
                OperatorCodeTextField.Dispose ();
                OperatorCodeTextField = null;
            }

            if (PhoneNumberTextField != null) {
                PhoneNumberTextField.Dispose ();
                PhoneNumberTextField = null;
            }
        }
    }
}