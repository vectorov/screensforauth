﻿using System;

using Foundation;
using UIKit;

namespace ScreensForAuth.iOS
{
	public partial class SwitchWithLabelCell : UITableViewCell
	{
		public static readonly NSString Key = new NSString("SwitchWithLabelCell");
		public static readonly UINib Nib;

		static SwitchWithLabelCell()
		{
			Nib = UINib.FromName("SwitchWithLabelCell", NSBundle.MainBundle);
		}

		protected SwitchWithLabelCell(IntPtr handle) : base(handle)
		{
			// Note: this .ctor should not contain any initialization logic.
		}

	}
}
