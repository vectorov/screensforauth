﻿using System;
using CoreGraphics;
using Foundation;
using UIKit;

namespace ScreensForAuth.iOS
{
	public enum SocialButtonName
	{
		None 			= 0,
		Facebook 		= 1,
		Vkontakte 		= 2,
		Odnoklassniki 	= 3,
		GooglePlus 		= 4,
	}

	public partial class SocialButtonsCell : UITableViewCell
	{
		public static readonly NSString Key = new NSString("SocialButtonsCell");
		public static readonly UINib Nib;

		public Action<SocialButtonName> Touch;

		static SocialButtonsCell()
		{
			Nib = UINib.FromName("SocialButtonsCell", NSBundle.MainBundle);
		}

		protected SocialButtonsCell(IntPtr handle) : base(handle)
		{
			// Note: this .ctor should not contain any initialization logic.
		}

		public override void AwakeFromNib()
		{
			base.AwakeFromNib();

			var margin = UIScreen.MainScreen.Bounds.Size.Width / 16;

			var w = (320 - 100) / 4;
			var cornerRadius = w / 2f;
			var widthBorder = 2f;

			SocialButtonWidth.Constant = w;

			MarginLeft1.Constant = (UIScreen.MainScreen.Bounds.Size.Width - margin * 3 - w * 4) / 2;
			MarginLeft2.Constant = margin;
			MarginLeft3.Constant = margin;
			MarginLeft4.Constant = margin;

			TitleLabel.Text = "SocialButtonsTitle".FromResource();

			ButtonFB.Layer.CornerRadius = cornerRadius;
			ButtonFB.Layer.BorderWidth = widthBorder;
			ButtonFB.Layer.BorderColor = ButtonFB.TintColor.CGColor;
			ButtonFB.ClipsToBounds = true;

			ButtonVK.Layer.CornerRadius = cornerRadius;
			ButtonVK.Layer.BorderWidth = widthBorder;
			ButtonVK.Layer.BorderColor = ButtonVK.TintColor.CGColor;
			ButtonVK.ClipsToBounds = true;

			ButtonOK.Layer.CornerRadius = cornerRadius;
			ButtonOK.Layer.BorderWidth = widthBorder;
			ButtonOK.Layer.BorderColor = ButtonOK.TintColor.CGColor;
			ButtonOK.ClipsToBounds = true;

			ButtonGooglePlus.Layer.CornerRadius = cornerRadius;
			ButtonGooglePlus.Layer.BorderWidth = widthBorder;
			ButtonGooglePlus.Layer.BorderColor = ButtonGooglePlus.TintColor.CGColor;
			ButtonGooglePlus.ClipsToBounds = true;

		}

		public override void Draw(CoreGraphics.CGRect rect)
		{
			base.Draw(rect);

			var startingPoint = new CGPoint(rect.GetMinX(), rect.GetMaxY()-5);

			var endingPoint = new CGPoint(rect.GetMaxX(), rect.GetMaxY()-5);

			var path = new UIBezierPath();

			path.MoveTo(startingPoint);
			path.AddLineTo(endingPoint);
			path.LineWidth = 1.0f;

			UIColor.LightGray.SetStroke();

			path.Stroke();

			MarginLeft1.Constant = (UIScreen.MainScreen.Bounds.Size.Width - MarginLeft2.Constant * 3 - SocialButtonWidth.Constant * 4) / 2;
		}

		partial void Button_TouchUpInside(UIButton sender)
		{
			if (Touch != null)
			{
				SocialButtonName nameButton = SocialButtonName.None;

				if (sender == ButtonFB)
					nameButton = SocialButtonName.Facebook;
				else if (sender == ButtonVK)
					nameButton = SocialButtonName.Vkontakte;
				else if (sender == ButtonOK)
					nameButton = SocialButtonName.Odnoklassniki;
				else if (sender == ButtonGooglePlus)
					nameButton = SocialButtonName.GooglePlus;

				Touch(nameButton);
			}
		}
	}
}
