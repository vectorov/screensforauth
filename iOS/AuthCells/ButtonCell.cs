﻿using System;

using Foundation;
using UIKit;

namespace ScreensForAuth.iOS
{
	public partial class ButtonCell : UITableViewCell
	{
		public static readonly NSString Key = new NSString("ButtonCell");
		public static readonly UINib Nib;

		public Action touch;

		static ButtonCell()
		{
			Nib = UINib.FromName("ButtonCell", NSBundle.MainBundle);
		}

		protected ButtonCell(IntPtr handle) : base(handle)
		{
			// Note: this .ctor should not contain any initialization logic.
		}

		public override void AwakeFromNib()
		{
			base.AwakeFromNib();
		}

		public void ConfigButton(ButtonConfig config)
		{ 
			Button.BackgroundColor = config.Color;
			Button.SetTitle(config.Title, UIControlState.Normal);
			buttonWidthConstraint.Constant = UIScreen.MainScreen.Bounds.Size.Width * config.WidthInPercent;
		}

		partial void Button_TouchUpInside(UIButton sender)
		{
			if (touch != null)
			{
				touch();
			}
		}
	}
}
