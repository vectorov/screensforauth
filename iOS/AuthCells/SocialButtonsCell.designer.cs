// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace ScreensForAuth.iOS
{
    [Register ("SocialButtonsCell")]
    partial class SocialButtonsCell
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton ButtonFB { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton ButtonGooglePlus { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton ButtonOK { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton ButtonVK { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint MarginLeft1 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint MarginLeft2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint MarginLeft3 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint MarginLeft4 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint SocialButtonWidth { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel TitleLabel { get; set; }

        [Action ("Button_TouchUpInside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void Button_TouchUpInside (UIKit.UIButton sender);

        void ReleaseDesignerOutlets ()
        {
            if (ButtonFB != null) {
                ButtonFB.Dispose ();
                ButtonFB = null;
            }

            if (ButtonGooglePlus != null) {
                ButtonGooglePlus.Dispose ();
                ButtonGooglePlus = null;
            }

            if (ButtonOK != null) {
                ButtonOK.Dispose ();
                ButtonOK = null;
            }

            if (ButtonVK != null) {
                ButtonVK.Dispose ();
                ButtonVK = null;
            }

            if (MarginLeft1 != null) {
                MarginLeft1.Dispose ();
                MarginLeft1 = null;
            }

            if (MarginLeft2 != null) {
                MarginLeft2.Dispose ();
                MarginLeft2 = null;
            }

            if (MarginLeft3 != null) {
                MarginLeft3.Dispose ();
                MarginLeft3 = null;
            }

            if (MarginLeft4 != null) {
                MarginLeft4.Dispose ();
                MarginLeft4 = null;
            }

            if (SocialButtonWidth != null) {
                SocialButtonWidth.Dispose ();
                SocialButtonWidth = null;
            }

            if (TitleLabel != null) {
                TitleLabel.Dispose ();
                TitleLabel = null;
            }
        }
    }
}