﻿using System;

using Foundation;
using UIKit;

namespace ScreensForAuth.iOS
{
	public partial class FogotPasswordCell : UITableViewCell
	{
		public static readonly NSString Key = new NSString("FogotPasswordCell");
		public static readonly UINib Nib;

		static FogotPasswordCell()
		{
			Nib = UINib.FromName("FogotPasswordCell", NSBundle.MainBundle);
		}

		protected FogotPasswordCell(IntPtr handle) : base(handle)
		{
			// Note: this .ctor should not contain any initialization logic.
		}

		partial void NextButton_TouchUpInside(UIButton sender)
		{
			throw new NotImplementedException();
		}
	}
}
