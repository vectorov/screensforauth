﻿using System;
using CoreAnimation;
using CoreGraphics;
using Foundation;
using UIKit;

namespace ScreensForAuth.iOS
{
	public partial class TextFieldCell : UITableViewCell
	{
		public static readonly NSString Key = new NSString("TextFieldCell");
		public static readonly UINib Nib;

		static TextFieldCell()
		{
			Nib = UINib.FromName("TextFieldCell", NSBundle.MainBundle);
		}

		protected TextFieldCell(IntPtr handle) : base(handle)
		{
			// Note: this .ctor should not contain any initialization logic.
		}

		public override void AwakeFromNib()
		{
			base.AwakeFromNib();
		}

		public void Config(TextFieldConfig config)
		{
			TextField.Placeholder = config.Placeholder;
			TextField.KeyboardType = config.KeyboardType;
			TextField.Text = config.Value;
			TextField.LeftView = config.LeftView;
			TextField.LeftViewMode = config.LeftViewMode;
			TextField.RightView = config.RightView;
			TextField.RightViewMode = config.RightViewMode;
			TextField.SecureTextEntry = config.Secure;

		}

		public override void Draw(CGRect rect)
		{
			base.Draw(rect);

			var MaxY = TextField.Frame.Y + TextField.Frame.Height + 2; //rect.GetMaxY() - 10;
			var startingPoint = new CGPoint(rect.GetMinX()+10, MaxY);

			var endingPoint = new CGPoint(rect.GetMaxX()-10, MaxY);

			var path = new UIBezierPath();

			path.MoveTo(startingPoint);
			path.AddLineTo(endingPoint);
			path.LineWidth = 1.0f;

			//TintColor.SetStroke();
			UIColor.LightGray.SetStroke();

			path.Stroke();
		}
	}
}
