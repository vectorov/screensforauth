// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace ScreensForAuth.iOS
{
    [Register ("RegistrationCell")]
    partial class RegistrationCell
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField CountryCodeTextField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField CountryTextField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel CustomerAgreementLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UISwitch CustomerSwitch { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField EmailTextField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton EnterButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel EnterTitleLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField NameTextField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField OperatorCodeTextField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField PhoneNumberTextField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton RegistrationButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField SurnameTextField { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (CountryCodeTextField != null) {
                CountryCodeTextField.Dispose ();
                CountryCodeTextField = null;
            }

            if (CountryTextField != null) {
                CountryTextField.Dispose ();
                CountryTextField = null;
            }

            if (CustomerAgreementLabel != null) {
                CustomerAgreementLabel.Dispose ();
                CustomerAgreementLabel = null;
            }

            if (CustomerSwitch != null) {
                CustomerSwitch.Dispose ();
                CustomerSwitch = null;
            }

            if (EmailTextField != null) {
                EmailTextField.Dispose ();
                EmailTextField = null;
            }

            if (EnterButton != null) {
                EnterButton.Dispose ();
                EnterButton = null;
            }

            if (EnterTitleLabel != null) {
                EnterTitleLabel.Dispose ();
                EnterTitleLabel = null;
            }

            if (NameTextField != null) {
                NameTextField.Dispose ();
                NameTextField = null;
            }

            if (OperatorCodeTextField != null) {
                OperatorCodeTextField.Dispose ();
                OperatorCodeTextField = null;
            }

            if (PhoneNumberTextField != null) {
                PhoneNumberTextField.Dispose ();
                PhoneNumberTextField = null;
            }

            if (RegistrationButton != null) {
                RegistrationButton.Dispose ();
                RegistrationButton = null;
            }

            if (SurnameTextField != null) {
                SurnameTextField.Dispose ();
                SurnameTextField = null;
            }
        }
    }
}