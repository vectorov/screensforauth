﻿using System;
namespace ScreensForAuth.iOS
{
	public class PickerChangedEventArgs : EventArgs
	{
		public string SelectedValue { get; set; }
	}
}
