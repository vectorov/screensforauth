﻿using System;
using System.Collections.Generic;
using CoreGraphics;
using Foundation;
using UIKit;

namespace ScreensForAuth.iOS
{
	public partial class RegistrationCell : UICollectionViewCell
	{
		public static readonly NSString Key = new NSString("RegistrationCell");
		public static readonly UINib Nib;

		private readonly IList<string> colors = new List<string>
		{
			"Blue",
			"Green",
			"Red",
			"Purple",
			"Yellow"
		};
		private string selectedColor;

		static RegistrationCell()
		{
			Nib = UINib.FromName("RegistrationCell", NSBundle.MainBundle);
		}

		protected RegistrationCell(IntPtr handle) : base(handle)
		{
			// Note: this .ctor should not contain any initialization logic.
		}

		public override void AwakeFromNib()
		{
			CustomerSwitch.Transform = CGAffineTransform.MakeScale(0.7f, 0.7f);
			base.AwakeFromNib();
		}

		public void SetupView()
		{
			NameTextField.Placeholder = "Name_RegistrPlaceholder".FromResource();
			NameTextField.LeftViewMode = UITextFieldViewMode.UnlessEditing;
			NameTextField.LeftView = new UIImageView(UIImage.FromBundle("PersonWhiteIcon"));

			SurnameTextField.Placeholder = "Surname_RegistrPlaceholder".FromResource();
			SurnameTextField.LeftViewMode = UITextFieldViewMode.UnlessEditing;
			SurnameTextField.LeftView = new UIImageView(UIImage.FromBundle("PersonWhiteIcon"));

			EmailTextField.Placeholder = "Email_RegistrPlaceholder".FromResource();
			EmailTextField.LeftViewMode = UITextFieldViewMode.UnlessEditing;
			EmailTextField.LeftView = new UIImageView(UIImage.FromBundle("EmailWhiteicon"));


			//CountryTextField.InputView = 
			SetupPicker();

			CountryCodeTextField.Placeholder = "CountryCode_RegistrPlaceholder".FromResource();
			OperatorCodeTextField.Placeholder = "OperatorCode_RegistrPlaceholder".FromResource();

			PhoneNumberTextField.Placeholder = "PhoneNumber_RegistrPlaceholder".FromResource();
			PhoneNumberTextField.LeftViewMode = UITextFieldViewMode.UnlessEditing;
			PhoneNumberTextField.LeftView = new UIImageView(UIImage.FromBundle("PhoneWhiteIcon"));

			CustomerAgreementLabel.AttributedText = new NSAttributedString("CustomerAgreement_RegistrLabel".FromResource());

			RegistrationButton.SetTitle("RegistrationButton_RegistrTitle".FromResource(), UIControlState.Normal);

			EnterTitleLabel.Text = "EnterTitle_RegistrLabel".FromResource();
			EnterButton.SetTitle("EnterButton_RegistrTitle".FromResource(), UIControlState.Normal);

		}

		private void SetupPicker()
		{
			// Setup the picker and model
			PickerModel model = new PickerModel(this.colors);
			model.PickerChanged += (sender, e) =>
			{
				this.selectedColor = e.SelectedValue;
				this.CountryTextField.Text = selectedColor;
			};

			UIPickerView picker = new UIPickerView();
			picker.ShowSelectionIndicator = true;
			picker.Model = model;

			// Setup the toolbar
			UIToolbar toolbar = new UIToolbar();
			toolbar.BarStyle = UIBarStyle.BlackTranslucent;
			toolbar.Translucent = true;
			toolbar.SizeToFit();

			UIBarButtonItem flexibleSpace = new UIBarButtonItem(UIBarButtonSystemItem.FlexibleSpace);
			// Create a 'done' button for the toolbar and add it to the toolbar
			UIBarButtonItem doneButton = 
				new UIBarButtonItem(
					UIBarButtonSystemItem.Stop, 
					 (s, e) =>
					 {
						this.CountryTextField.Text = selectedColor;
						this.CountryCodeTextField.Text = colors.IndexOf(selectedColor).ToString();
						this.CountryTextField.ResignFirstResponder();
					 }
			);
			toolbar.SetItems(new UIBarButtonItem[] { flexibleSpace, doneButton }, true);

			// Tell the textbox to use the picker for input
			this.CountryTextField.InputView = picker;

			// Display the toolbar over the pickers
			this.CountryTextField.InputAccessoryView = toolbar;
		}
	}
}
